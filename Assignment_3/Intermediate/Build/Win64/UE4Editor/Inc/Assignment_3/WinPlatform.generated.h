// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef ASSIGNMENT_3_WinPlatform_generated_h
#error "WinPlatform.generated.h already included, missing '#pragma once' in WinPlatform.h"
#endif
#define ASSIGNMENT_3_WinPlatform_generated_h

#define Assignment_3_Source_Assignment_3_WinPlatform_h_14_SPARSE_DATA
#define Assignment_3_Source_Assignment_3_WinPlatform_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnPlayerOverlapBegin);


#define Assignment_3_Source_Assignment_3_WinPlatform_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnPlayerOverlapBegin);


#define Assignment_3_Source_Assignment_3_WinPlatform_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWinPlatform(); \
	friend struct Z_Construct_UClass_AWinPlatform_Statics; \
public: \
	DECLARE_CLASS(AWinPlatform, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment_3"), NO_API) \
	DECLARE_SERIALIZER(AWinPlatform)


#define Assignment_3_Source_Assignment_3_WinPlatform_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAWinPlatform(); \
	friend struct Z_Construct_UClass_AWinPlatform_Statics; \
public: \
	DECLARE_CLASS(AWinPlatform, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment_3"), NO_API) \
	DECLARE_SERIALIZER(AWinPlatform)


#define Assignment_3_Source_Assignment_3_WinPlatform_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWinPlatform(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWinPlatform) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWinPlatform); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWinPlatform); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWinPlatform(AWinPlatform&&); \
	NO_API AWinPlatform(const AWinPlatform&); \
public:


#define Assignment_3_Source_Assignment_3_WinPlatform_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWinPlatform(AWinPlatform&&); \
	NO_API AWinPlatform(const AWinPlatform&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWinPlatform); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWinPlatform); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWinPlatform)


#define Assignment_3_Source_Assignment_3_WinPlatform_h_14_PRIVATE_PROPERTY_OFFSET
#define Assignment_3_Source_Assignment_3_WinPlatform_h_11_PROLOG
#define Assignment_3_Source_Assignment_3_WinPlatform_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment_3_Source_Assignment_3_WinPlatform_h_14_PRIVATE_PROPERTY_OFFSET \
	Assignment_3_Source_Assignment_3_WinPlatform_h_14_SPARSE_DATA \
	Assignment_3_Source_Assignment_3_WinPlatform_h_14_RPC_WRAPPERS \
	Assignment_3_Source_Assignment_3_WinPlatform_h_14_INCLASS \
	Assignment_3_Source_Assignment_3_WinPlatform_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Assignment_3_Source_Assignment_3_WinPlatform_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment_3_Source_Assignment_3_WinPlatform_h_14_PRIVATE_PROPERTY_OFFSET \
	Assignment_3_Source_Assignment_3_WinPlatform_h_14_SPARSE_DATA \
	Assignment_3_Source_Assignment_3_WinPlatform_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Assignment_3_Source_Assignment_3_WinPlatform_h_14_INCLASS_NO_PURE_DECLS \
	Assignment_3_Source_Assignment_3_WinPlatform_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASSIGNMENT_3_API UClass* StaticClass<class AWinPlatform>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Assignment_3_Source_Assignment_3_WinPlatform_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
