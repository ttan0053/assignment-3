// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Assignment_3/Assignment_3HUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAssignment_3HUD() {}
// Cross Module References
	ASSIGNMENT_3_API UClass* Z_Construct_UClass_AAssignment_3HUD_NoRegister();
	ASSIGNMENT_3_API UClass* Z_Construct_UClass_AAssignment_3HUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_Assignment_3();
// End Cross Module References
	void AAssignment_3HUD::StaticRegisterNativesAAssignment_3HUD()
	{
	}
	UClass* Z_Construct_UClass_AAssignment_3HUD_NoRegister()
	{
		return AAssignment_3HUD::StaticClass();
	}
	struct Z_Construct_UClass_AAssignment_3HUD_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AAssignment_3HUD_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AHUD,
		(UObject* (*)())Z_Construct_UPackage__Script_Assignment_3,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAssignment_3HUD_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Rendering Actor Input Replication" },
		{ "IncludePath", "Assignment_3HUD.h" },
		{ "ModuleRelativePath", "Assignment_3HUD.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AAssignment_3HUD_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AAssignment_3HUD>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AAssignment_3HUD_Statics::ClassParams = {
		&AAssignment_3HUD::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AAssignment_3HUD_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AAssignment_3HUD_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AAssignment_3HUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AAssignment_3HUD_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AAssignment_3HUD, 2503910476);
	template<> ASSIGNMENT_3_API UClass* StaticClass<AAssignment_3HUD>()
	{
		return AAssignment_3HUD::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AAssignment_3HUD(Z_Construct_UClass_AAssignment_3HUD, &AAssignment_3HUD::StaticClass, TEXT("/Script/Assignment_3"), TEXT("AAssignment_3HUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AAssignment_3HUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
