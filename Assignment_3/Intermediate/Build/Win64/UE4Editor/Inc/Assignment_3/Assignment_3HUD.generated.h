// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASSIGNMENT_3_Assignment_3HUD_generated_h
#error "Assignment_3HUD.generated.h already included, missing '#pragma once' in Assignment_3HUD.h"
#endif
#define ASSIGNMENT_3_Assignment_3HUD_generated_h

#define Assignment_3_Source_Assignment_3_Assignment_3HUD_h_12_SPARSE_DATA
#define Assignment_3_Source_Assignment_3_Assignment_3HUD_h_12_RPC_WRAPPERS
#define Assignment_3_Source_Assignment_3_Assignment_3HUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Assignment_3_Source_Assignment_3_Assignment_3HUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAssignment_3HUD(); \
	friend struct Z_Construct_UClass_AAssignment_3HUD_Statics; \
public: \
	DECLARE_CLASS(AAssignment_3HUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment_3"), NO_API) \
	DECLARE_SERIALIZER(AAssignment_3HUD)


#define Assignment_3_Source_Assignment_3_Assignment_3HUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAAssignment_3HUD(); \
	friend struct Z_Construct_UClass_AAssignment_3HUD_Statics; \
public: \
	DECLARE_CLASS(AAssignment_3HUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment_3"), NO_API) \
	DECLARE_SERIALIZER(AAssignment_3HUD)


#define Assignment_3_Source_Assignment_3_Assignment_3HUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAssignment_3HUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAssignment_3HUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAssignment_3HUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAssignment_3HUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAssignment_3HUD(AAssignment_3HUD&&); \
	NO_API AAssignment_3HUD(const AAssignment_3HUD&); \
public:


#define Assignment_3_Source_Assignment_3_Assignment_3HUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAssignment_3HUD(AAssignment_3HUD&&); \
	NO_API AAssignment_3HUD(const AAssignment_3HUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAssignment_3HUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAssignment_3HUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAssignment_3HUD)


#define Assignment_3_Source_Assignment_3_Assignment_3HUD_h_12_PRIVATE_PROPERTY_OFFSET
#define Assignment_3_Source_Assignment_3_Assignment_3HUD_h_9_PROLOG
#define Assignment_3_Source_Assignment_3_Assignment_3HUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment_3_Source_Assignment_3_Assignment_3HUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Assignment_3_Source_Assignment_3_Assignment_3HUD_h_12_SPARSE_DATA \
	Assignment_3_Source_Assignment_3_Assignment_3HUD_h_12_RPC_WRAPPERS \
	Assignment_3_Source_Assignment_3_Assignment_3HUD_h_12_INCLASS \
	Assignment_3_Source_Assignment_3_Assignment_3HUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Assignment_3_Source_Assignment_3_Assignment_3HUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment_3_Source_Assignment_3_Assignment_3HUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Assignment_3_Source_Assignment_3_Assignment_3HUD_h_12_SPARSE_DATA \
	Assignment_3_Source_Assignment_3_Assignment_3HUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Assignment_3_Source_Assignment_3_Assignment_3HUD_h_12_INCLASS_NO_PURE_DECLS \
	Assignment_3_Source_Assignment_3_Assignment_3HUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASSIGNMENT_3_API UClass* StaticClass<class AAssignment_3HUD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Assignment_3_Source_Assignment_3_Assignment_3HUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
