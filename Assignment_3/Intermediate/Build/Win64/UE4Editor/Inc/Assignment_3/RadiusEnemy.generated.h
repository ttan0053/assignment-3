// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef ASSIGNMENT_3_RadiusEnemy_generated_h
#error "RadiusEnemy.generated.h already included, missing '#pragma once' in RadiusEnemy.h"
#endif
#define ASSIGNMENT_3_RadiusEnemy_generated_h

#define Assignment_3_Source_Assignment_3_RadiusEnemy_h_14_SPARSE_DATA
#define Assignment_3_Source_Assignment_3_RadiusEnemy_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnPlayerOverlapEnd); \
	DECLARE_FUNCTION(execOnPlayerOverlapBegin); \
	DECLARE_FUNCTION(execOnProjectileOverlapBegin);


#define Assignment_3_Source_Assignment_3_RadiusEnemy_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnPlayerOverlapEnd); \
	DECLARE_FUNCTION(execOnPlayerOverlapBegin); \
	DECLARE_FUNCTION(execOnProjectileOverlapBegin);


#define Assignment_3_Source_Assignment_3_RadiusEnemy_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARadiusEnemy(); \
	friend struct Z_Construct_UClass_ARadiusEnemy_Statics; \
public: \
	DECLARE_CLASS(ARadiusEnemy, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment_3"), NO_API) \
	DECLARE_SERIALIZER(ARadiusEnemy)


#define Assignment_3_Source_Assignment_3_RadiusEnemy_h_14_INCLASS \
private: \
	static void StaticRegisterNativesARadiusEnemy(); \
	friend struct Z_Construct_UClass_ARadiusEnemy_Statics; \
public: \
	DECLARE_CLASS(ARadiusEnemy, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment_3"), NO_API) \
	DECLARE_SERIALIZER(ARadiusEnemy)


#define Assignment_3_Source_Assignment_3_RadiusEnemy_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARadiusEnemy(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARadiusEnemy) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARadiusEnemy); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARadiusEnemy); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARadiusEnemy(ARadiusEnemy&&); \
	NO_API ARadiusEnemy(const ARadiusEnemy&); \
public:


#define Assignment_3_Source_Assignment_3_RadiusEnemy_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARadiusEnemy(ARadiusEnemy&&); \
	NO_API ARadiusEnemy(const ARadiusEnemy&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARadiusEnemy); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARadiusEnemy); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARadiusEnemy)


#define Assignment_3_Source_Assignment_3_RadiusEnemy_h_14_PRIVATE_PROPERTY_OFFSET
#define Assignment_3_Source_Assignment_3_RadiusEnemy_h_11_PROLOG
#define Assignment_3_Source_Assignment_3_RadiusEnemy_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment_3_Source_Assignment_3_RadiusEnemy_h_14_PRIVATE_PROPERTY_OFFSET \
	Assignment_3_Source_Assignment_3_RadiusEnemy_h_14_SPARSE_DATA \
	Assignment_3_Source_Assignment_3_RadiusEnemy_h_14_RPC_WRAPPERS \
	Assignment_3_Source_Assignment_3_RadiusEnemy_h_14_INCLASS \
	Assignment_3_Source_Assignment_3_RadiusEnemy_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Assignment_3_Source_Assignment_3_RadiusEnemy_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment_3_Source_Assignment_3_RadiusEnemy_h_14_PRIVATE_PROPERTY_OFFSET \
	Assignment_3_Source_Assignment_3_RadiusEnemy_h_14_SPARSE_DATA \
	Assignment_3_Source_Assignment_3_RadiusEnemy_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Assignment_3_Source_Assignment_3_RadiusEnemy_h_14_INCLASS_NO_PURE_DECLS \
	Assignment_3_Source_Assignment_3_RadiusEnemy_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASSIGNMENT_3_API UClass* StaticClass<class ARadiusEnemy>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Assignment_3_Source_Assignment_3_RadiusEnemy_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
