// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASSIGNMENT_3_SpawnShooterEnemy_generated_h
#error "SpawnShooterEnemy.generated.h already included, missing '#pragma once' in SpawnShooterEnemy.h"
#endif
#define ASSIGNMENT_3_SpawnShooterEnemy_generated_h

#define Assignment_3_Source_Assignment_3_SpawnShooterEnemy_h_12_SPARSE_DATA
#define Assignment_3_Source_Assignment_3_SpawnShooterEnemy_h_12_RPC_WRAPPERS
#define Assignment_3_Source_Assignment_3_SpawnShooterEnemy_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Assignment_3_Source_Assignment_3_SpawnShooterEnemy_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpawnShooterEnemy(); \
	friend struct Z_Construct_UClass_ASpawnShooterEnemy_Statics; \
public: \
	DECLARE_CLASS(ASpawnShooterEnemy, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment_3"), NO_API) \
	DECLARE_SERIALIZER(ASpawnShooterEnemy)


#define Assignment_3_Source_Assignment_3_SpawnShooterEnemy_h_12_INCLASS \
private: \
	static void StaticRegisterNativesASpawnShooterEnemy(); \
	friend struct Z_Construct_UClass_ASpawnShooterEnemy_Statics; \
public: \
	DECLARE_CLASS(ASpawnShooterEnemy, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment_3"), NO_API) \
	DECLARE_SERIALIZER(ASpawnShooterEnemy)


#define Assignment_3_Source_Assignment_3_SpawnShooterEnemy_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpawnShooterEnemy(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpawnShooterEnemy) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawnShooterEnemy); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawnShooterEnemy); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawnShooterEnemy(ASpawnShooterEnemy&&); \
	NO_API ASpawnShooterEnemy(const ASpawnShooterEnemy&); \
public:


#define Assignment_3_Source_Assignment_3_SpawnShooterEnemy_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawnShooterEnemy(ASpawnShooterEnemy&&); \
	NO_API ASpawnShooterEnemy(const ASpawnShooterEnemy&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawnShooterEnemy); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawnShooterEnemy); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpawnShooterEnemy)


#define Assignment_3_Source_Assignment_3_SpawnShooterEnemy_h_12_PRIVATE_PROPERTY_OFFSET
#define Assignment_3_Source_Assignment_3_SpawnShooterEnemy_h_9_PROLOG
#define Assignment_3_Source_Assignment_3_SpawnShooterEnemy_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment_3_Source_Assignment_3_SpawnShooterEnemy_h_12_PRIVATE_PROPERTY_OFFSET \
	Assignment_3_Source_Assignment_3_SpawnShooterEnemy_h_12_SPARSE_DATA \
	Assignment_3_Source_Assignment_3_SpawnShooterEnemy_h_12_RPC_WRAPPERS \
	Assignment_3_Source_Assignment_3_SpawnShooterEnemy_h_12_INCLASS \
	Assignment_3_Source_Assignment_3_SpawnShooterEnemy_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Assignment_3_Source_Assignment_3_SpawnShooterEnemy_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment_3_Source_Assignment_3_SpawnShooterEnemy_h_12_PRIVATE_PROPERTY_OFFSET \
	Assignment_3_Source_Assignment_3_SpawnShooterEnemy_h_12_SPARSE_DATA \
	Assignment_3_Source_Assignment_3_SpawnShooterEnemy_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Assignment_3_Source_Assignment_3_SpawnShooterEnemy_h_12_INCLASS_NO_PURE_DECLS \
	Assignment_3_Source_Assignment_3_SpawnShooterEnemy_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASSIGNMENT_3_API UClass* StaticClass<class ASpawnShooterEnemy>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Assignment_3_Source_Assignment_3_SpawnShooterEnemy_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
