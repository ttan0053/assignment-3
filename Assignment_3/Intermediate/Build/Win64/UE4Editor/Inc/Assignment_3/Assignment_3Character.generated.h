// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASSIGNMENT_3_Assignment_3Character_generated_h
#error "Assignment_3Character.generated.h already included, missing '#pragma once' in Assignment_3Character.h"
#endif
#define ASSIGNMENT_3_Assignment_3Character_generated_h

#define Assignment_3_Source_Assignment_3_Assignment_3Character_h_23_SPARSE_DATA
#define Assignment_3_Source_Assignment_3_Assignment_3Character_h_23_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execPurchaseSpeed); \
	DECLARE_FUNCTION(execPurchaseHealth); \
	DECLARE_FUNCTION(execGetTotalCoins); \
	DECLARE_FUNCTION(execGetCoints); \
	DECLARE_FUNCTION(execGetKills); \
	DECLARE_FUNCTION(execUpdateKills); \
	DECLARE_FUNCTION(execUpdateHealth); \
	DECLARE_FUNCTION(execGetHealth);


#define Assignment_3_Source_Assignment_3_Assignment_3Character_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execPurchaseSpeed); \
	DECLARE_FUNCTION(execPurchaseHealth); \
	DECLARE_FUNCTION(execGetTotalCoins); \
	DECLARE_FUNCTION(execGetCoints); \
	DECLARE_FUNCTION(execGetKills); \
	DECLARE_FUNCTION(execUpdateKills); \
	DECLARE_FUNCTION(execUpdateHealth); \
	DECLARE_FUNCTION(execGetHealth);


#define Assignment_3_Source_Assignment_3_Assignment_3Character_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAssignment_3Character(); \
	friend struct Z_Construct_UClass_AAssignment_3Character_Statics; \
public: \
	DECLARE_CLASS(AAssignment_3Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment_3"), NO_API) \
	DECLARE_SERIALIZER(AAssignment_3Character)


#define Assignment_3_Source_Assignment_3_Assignment_3Character_h_23_INCLASS \
private: \
	static void StaticRegisterNativesAAssignment_3Character(); \
	friend struct Z_Construct_UClass_AAssignment_3Character_Statics; \
public: \
	DECLARE_CLASS(AAssignment_3Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment_3"), NO_API) \
	DECLARE_SERIALIZER(AAssignment_3Character)


#define Assignment_3_Source_Assignment_3_Assignment_3Character_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAssignment_3Character(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAssignment_3Character) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAssignment_3Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAssignment_3Character); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAssignment_3Character(AAssignment_3Character&&); \
	NO_API AAssignment_3Character(const AAssignment_3Character&); \
public:


#define Assignment_3_Source_Assignment_3_Assignment_3Character_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAssignment_3Character(AAssignment_3Character&&); \
	NO_API AAssignment_3Character(const AAssignment_3Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAssignment_3Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAssignment_3Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAssignment_3Character)


#define Assignment_3_Source_Assignment_3_Assignment_3Character_h_23_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(AAssignment_3Character, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(AAssignment_3Character, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(AAssignment_3Character, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(AAssignment_3Character, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(AAssignment_3Character, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(AAssignment_3Character, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(AAssignment_3Character, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(AAssignment_3Character, L_MotionController); }


#define Assignment_3_Source_Assignment_3_Assignment_3Character_h_20_PROLOG
#define Assignment_3_Source_Assignment_3_Assignment_3Character_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment_3_Source_Assignment_3_Assignment_3Character_h_23_PRIVATE_PROPERTY_OFFSET \
	Assignment_3_Source_Assignment_3_Assignment_3Character_h_23_SPARSE_DATA \
	Assignment_3_Source_Assignment_3_Assignment_3Character_h_23_RPC_WRAPPERS \
	Assignment_3_Source_Assignment_3_Assignment_3Character_h_23_INCLASS \
	Assignment_3_Source_Assignment_3_Assignment_3Character_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Assignment_3_Source_Assignment_3_Assignment_3Character_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment_3_Source_Assignment_3_Assignment_3Character_h_23_PRIVATE_PROPERTY_OFFSET \
	Assignment_3_Source_Assignment_3_Assignment_3Character_h_23_SPARSE_DATA \
	Assignment_3_Source_Assignment_3_Assignment_3Character_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Assignment_3_Source_Assignment_3_Assignment_3Character_h_23_INCLASS_NO_PURE_DECLS \
	Assignment_3_Source_Assignment_3_Assignment_3Character_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASSIGNMENT_3_API UClass* StaticClass<class AAssignment_3Character>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Assignment_3_Source_Assignment_3_Assignment_3Character_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
