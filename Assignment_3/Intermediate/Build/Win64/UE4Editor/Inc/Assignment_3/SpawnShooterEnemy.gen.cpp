// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Assignment_3/SpawnShooterEnemy.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSpawnShooterEnemy() {}
// Cross Module References
	ASSIGNMENT_3_API UClass* Z_Construct_UClass_ASpawnShooterEnemy_NoRegister();
	ASSIGNMENT_3_API UClass* Z_Construct_UClass_ASpawnShooterEnemy();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Assignment_3();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	void ASpawnShooterEnemy::StaticRegisterNativesASpawnShooterEnemy()
	{
	}
	UClass* Z_Construct_UClass_ASpawnShooterEnemy_NoRegister()
	{
		return ASpawnShooterEnemy::StaticClass();
	}
	struct Z_Construct_UClass_ASpawnShooterEnemy_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnInterval_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SpawnInterval;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StartTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_StartTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxNumberOfSpawns_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_MaxNumberOfSpawns;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnObject1_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_SpawnObject1;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Checkpoints_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Checkpoints_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Checkpoints;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASpawnShooterEnemy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Assignment_3,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpawnShooterEnemy_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SpawnShooterEnemy.h" },
		{ "ModuleRelativePath", "SpawnShooterEnemy.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_SpawnInterval_MetaData[] = {
		{ "Category", "SpawnShooterEnemy" },
		{ "ModuleRelativePath", "SpawnShooterEnemy.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_SpawnInterval = { "SpawnInterval", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASpawnShooterEnemy, SpawnInterval), METADATA_PARAMS(Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_SpawnInterval_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_SpawnInterval_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_StartTime_MetaData[] = {
		{ "Category", "SpawnShooterEnemy" },
		{ "ModuleRelativePath", "SpawnShooterEnemy.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_StartTime = { "StartTime", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASpawnShooterEnemy, StartTime), METADATA_PARAMS(Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_StartTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_StartTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_MaxNumberOfSpawns_MetaData[] = {
		{ "Category", "SpawnShooterEnemy" },
		{ "ModuleRelativePath", "SpawnShooterEnemy.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_MaxNumberOfSpawns = { "MaxNumberOfSpawns", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASpawnShooterEnemy, MaxNumberOfSpawns), METADATA_PARAMS(Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_MaxNumberOfSpawns_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_MaxNumberOfSpawns_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_SpawnObject1_MetaData[] = {
		{ "Category", "SpawnObject" },
		{ "ModuleRelativePath", "SpawnShooterEnemy.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_SpawnObject1 = { "SpawnObject1", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASpawnShooterEnemy, SpawnObject1), Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_SpawnObject1_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_SpawnObject1_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_Checkpoints_Inner = { "Checkpoints", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_Checkpoints_MetaData[] = {
		{ "Category", "SpawnShooterEnemy" },
		{ "ModuleRelativePath", "SpawnShooterEnemy.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_Checkpoints = { "Checkpoints", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASpawnShooterEnemy, Checkpoints), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_Checkpoints_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_Checkpoints_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ASpawnShooterEnemy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_SpawnInterval,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_StartTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_MaxNumberOfSpawns,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_SpawnObject1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_Checkpoints_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASpawnShooterEnemy_Statics::NewProp_Checkpoints,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASpawnShooterEnemy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASpawnShooterEnemy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASpawnShooterEnemy_Statics::ClassParams = {
		&ASpawnShooterEnemy::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ASpawnShooterEnemy_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnShooterEnemy_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ASpawnShooterEnemy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnShooterEnemy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASpawnShooterEnemy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASpawnShooterEnemy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASpawnShooterEnemy, 2484307115);
	template<> ASSIGNMENT_3_API UClass* StaticClass<ASpawnShooterEnemy>()
	{
		return ASpawnShooterEnemy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASpawnShooterEnemy(Z_Construct_UClass_ASpawnShooterEnemy, &ASpawnShooterEnemy::StaticClass, TEXT("/Script/Assignment_3"), TEXT("ASpawnShooterEnemy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASpawnShooterEnemy);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
