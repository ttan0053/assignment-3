// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASSIGNMENT_3_Assignment_3GameMode_generated_h
#error "Assignment_3GameMode.generated.h already included, missing '#pragma once' in Assignment_3GameMode.h"
#endif
#define ASSIGNMENT_3_Assignment_3GameMode_generated_h

#define Assignment_3_Source_Assignment_3_Assignment_3GameMode_h_12_SPARSE_DATA
#define Assignment_3_Source_Assignment_3_Assignment_3GameMode_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetElapsedTime);


#define Assignment_3_Source_Assignment_3_Assignment_3GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetElapsedTime);


#define Assignment_3_Source_Assignment_3_Assignment_3GameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAssignment_3GameMode(); \
	friend struct Z_Construct_UClass_AAssignment_3GameMode_Statics; \
public: \
	DECLARE_CLASS(AAssignment_3GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment_3"), ASSIGNMENT_3_API) \
	DECLARE_SERIALIZER(AAssignment_3GameMode)


#define Assignment_3_Source_Assignment_3_Assignment_3GameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAAssignment_3GameMode(); \
	friend struct Z_Construct_UClass_AAssignment_3GameMode_Statics; \
public: \
	DECLARE_CLASS(AAssignment_3GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment_3"), ASSIGNMENT_3_API) \
	DECLARE_SERIALIZER(AAssignment_3GameMode)


#define Assignment_3_Source_Assignment_3_Assignment_3GameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ASSIGNMENT_3_API AAssignment_3GameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAssignment_3GameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ASSIGNMENT_3_API, AAssignment_3GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAssignment_3GameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ASSIGNMENT_3_API AAssignment_3GameMode(AAssignment_3GameMode&&); \
	ASSIGNMENT_3_API AAssignment_3GameMode(const AAssignment_3GameMode&); \
public:


#define Assignment_3_Source_Assignment_3_Assignment_3GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ASSIGNMENT_3_API AAssignment_3GameMode(AAssignment_3GameMode&&); \
	ASSIGNMENT_3_API AAssignment_3GameMode(const AAssignment_3GameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ASSIGNMENT_3_API, AAssignment_3GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAssignment_3GameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAssignment_3GameMode)


#define Assignment_3_Source_Assignment_3_Assignment_3GameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define Assignment_3_Source_Assignment_3_Assignment_3GameMode_h_9_PROLOG
#define Assignment_3_Source_Assignment_3_Assignment_3GameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment_3_Source_Assignment_3_Assignment_3GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Assignment_3_Source_Assignment_3_Assignment_3GameMode_h_12_SPARSE_DATA \
	Assignment_3_Source_Assignment_3_Assignment_3GameMode_h_12_RPC_WRAPPERS \
	Assignment_3_Source_Assignment_3_Assignment_3GameMode_h_12_INCLASS \
	Assignment_3_Source_Assignment_3_Assignment_3GameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Assignment_3_Source_Assignment_3_Assignment_3GameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment_3_Source_Assignment_3_Assignment_3GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Assignment_3_Source_Assignment_3_Assignment_3GameMode_h_12_SPARSE_DATA \
	Assignment_3_Source_Assignment_3_Assignment_3GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Assignment_3_Source_Assignment_3_Assignment_3GameMode_h_12_INCLASS_NO_PURE_DECLS \
	Assignment_3_Source_Assignment_3_Assignment_3GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASSIGNMENT_3_API UClass* StaticClass<class AAssignment_3GameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Assignment_3_Source_Assignment_3_Assignment_3GameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
