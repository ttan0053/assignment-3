// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef ASSIGNMENT_3_Assignment_3Projectile_generated_h
#error "Assignment_3Projectile.generated.h already included, missing '#pragma once' in Assignment_3Projectile.h"
#endif
#define ASSIGNMENT_3_Assignment_3Projectile_generated_h

#define Assignment_3_Source_Assignment_3_Assignment_3Projectile_h_15_SPARSE_DATA
#define Assignment_3_Source_Assignment_3_Assignment_3Projectile_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit);


#define Assignment_3_Source_Assignment_3_Assignment_3Projectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit);


#define Assignment_3_Source_Assignment_3_Assignment_3Projectile_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAssignment_3Projectile(); \
	friend struct Z_Construct_UClass_AAssignment_3Projectile_Statics; \
public: \
	DECLARE_CLASS(AAssignment_3Projectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment_3"), NO_API) \
	DECLARE_SERIALIZER(AAssignment_3Projectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Assignment_3_Source_Assignment_3_Assignment_3Projectile_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAAssignment_3Projectile(); \
	friend struct Z_Construct_UClass_AAssignment_3Projectile_Statics; \
public: \
	DECLARE_CLASS(AAssignment_3Projectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment_3"), NO_API) \
	DECLARE_SERIALIZER(AAssignment_3Projectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Assignment_3_Source_Assignment_3_Assignment_3Projectile_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAssignment_3Projectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAssignment_3Projectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAssignment_3Projectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAssignment_3Projectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAssignment_3Projectile(AAssignment_3Projectile&&); \
	NO_API AAssignment_3Projectile(const AAssignment_3Projectile&); \
public:


#define Assignment_3_Source_Assignment_3_Assignment_3Projectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAssignment_3Projectile(AAssignment_3Projectile&&); \
	NO_API AAssignment_3Projectile(const AAssignment_3Projectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAssignment_3Projectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAssignment_3Projectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAssignment_3Projectile)


#define Assignment_3_Source_Assignment_3_Assignment_3Projectile_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(AAssignment_3Projectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(AAssignment_3Projectile, ProjectileMovement); }


#define Assignment_3_Source_Assignment_3_Assignment_3Projectile_h_12_PROLOG
#define Assignment_3_Source_Assignment_3_Assignment_3Projectile_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment_3_Source_Assignment_3_Assignment_3Projectile_h_15_PRIVATE_PROPERTY_OFFSET \
	Assignment_3_Source_Assignment_3_Assignment_3Projectile_h_15_SPARSE_DATA \
	Assignment_3_Source_Assignment_3_Assignment_3Projectile_h_15_RPC_WRAPPERS \
	Assignment_3_Source_Assignment_3_Assignment_3Projectile_h_15_INCLASS \
	Assignment_3_Source_Assignment_3_Assignment_3Projectile_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Assignment_3_Source_Assignment_3_Assignment_3Projectile_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment_3_Source_Assignment_3_Assignment_3Projectile_h_15_PRIVATE_PROPERTY_OFFSET \
	Assignment_3_Source_Assignment_3_Assignment_3Projectile_h_15_SPARSE_DATA \
	Assignment_3_Source_Assignment_3_Assignment_3Projectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Assignment_3_Source_Assignment_3_Assignment_3Projectile_h_15_INCLASS_NO_PURE_DECLS \
	Assignment_3_Source_Assignment_3_Assignment_3Projectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASSIGNMENT_3_API UClass* StaticClass<class AAssignment_3Projectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Assignment_3_Source_Assignment_3_Assignment_3Projectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
