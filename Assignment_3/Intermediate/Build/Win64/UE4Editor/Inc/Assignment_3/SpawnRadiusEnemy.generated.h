// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASSIGNMENT_3_SpawnRadiusEnemy_generated_h
#error "SpawnRadiusEnemy.generated.h already included, missing '#pragma once' in SpawnRadiusEnemy.h"
#endif
#define ASSIGNMENT_3_SpawnRadiusEnemy_generated_h

#define Assignment_3_Source_Assignment_3_SpawnRadiusEnemy_h_13_SPARSE_DATA
#define Assignment_3_Source_Assignment_3_SpawnRadiusEnemy_h_13_RPC_WRAPPERS
#define Assignment_3_Source_Assignment_3_SpawnRadiusEnemy_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Assignment_3_Source_Assignment_3_SpawnRadiusEnemy_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpawnRadiusEnemy(); \
	friend struct Z_Construct_UClass_ASpawnRadiusEnemy_Statics; \
public: \
	DECLARE_CLASS(ASpawnRadiusEnemy, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment_3"), NO_API) \
	DECLARE_SERIALIZER(ASpawnRadiusEnemy)


#define Assignment_3_Source_Assignment_3_SpawnRadiusEnemy_h_13_INCLASS \
private: \
	static void StaticRegisterNativesASpawnRadiusEnemy(); \
	friend struct Z_Construct_UClass_ASpawnRadiusEnemy_Statics; \
public: \
	DECLARE_CLASS(ASpawnRadiusEnemy, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment_3"), NO_API) \
	DECLARE_SERIALIZER(ASpawnRadiusEnemy)


#define Assignment_3_Source_Assignment_3_SpawnRadiusEnemy_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpawnRadiusEnemy(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpawnRadiusEnemy) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawnRadiusEnemy); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawnRadiusEnemy); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawnRadiusEnemy(ASpawnRadiusEnemy&&); \
	NO_API ASpawnRadiusEnemy(const ASpawnRadiusEnemy&); \
public:


#define Assignment_3_Source_Assignment_3_SpawnRadiusEnemy_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawnRadiusEnemy(ASpawnRadiusEnemy&&); \
	NO_API ASpawnRadiusEnemy(const ASpawnRadiusEnemy&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawnRadiusEnemy); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawnRadiusEnemy); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpawnRadiusEnemy)


#define Assignment_3_Source_Assignment_3_SpawnRadiusEnemy_h_13_PRIVATE_PROPERTY_OFFSET
#define Assignment_3_Source_Assignment_3_SpawnRadiusEnemy_h_10_PROLOG
#define Assignment_3_Source_Assignment_3_SpawnRadiusEnemy_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment_3_Source_Assignment_3_SpawnRadiusEnemy_h_13_PRIVATE_PROPERTY_OFFSET \
	Assignment_3_Source_Assignment_3_SpawnRadiusEnemy_h_13_SPARSE_DATA \
	Assignment_3_Source_Assignment_3_SpawnRadiusEnemy_h_13_RPC_WRAPPERS \
	Assignment_3_Source_Assignment_3_SpawnRadiusEnemy_h_13_INCLASS \
	Assignment_3_Source_Assignment_3_SpawnRadiusEnemy_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Assignment_3_Source_Assignment_3_SpawnRadiusEnemy_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment_3_Source_Assignment_3_SpawnRadiusEnemy_h_13_PRIVATE_PROPERTY_OFFSET \
	Assignment_3_Source_Assignment_3_SpawnRadiusEnemy_h_13_SPARSE_DATA \
	Assignment_3_Source_Assignment_3_SpawnRadiusEnemy_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Assignment_3_Source_Assignment_3_SpawnRadiusEnemy_h_13_INCLASS_NO_PURE_DECLS \
	Assignment_3_Source_Assignment_3_SpawnRadiusEnemy_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASSIGNMENT_3_API UClass* StaticClass<class ASpawnRadiusEnemy>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Assignment_3_Source_Assignment_3_SpawnRadiusEnemy_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
