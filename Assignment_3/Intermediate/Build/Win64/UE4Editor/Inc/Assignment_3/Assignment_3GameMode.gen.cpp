// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Assignment_3/Assignment_3GameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAssignment_3GameMode() {}
// Cross Module References
	ASSIGNMENT_3_API UClass* Z_Construct_UClass_AAssignment_3GameMode_NoRegister();
	ASSIGNMENT_3_API UClass* Z_Construct_UClass_AAssignment_3GameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Assignment_3();
// End Cross Module References
	DEFINE_FUNCTION(AAssignment_3GameMode::execGetElapsedTime)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetElapsedTime();
		P_NATIVE_END;
	}
	void AAssignment_3GameMode::StaticRegisterNativesAAssignment_3GameMode()
	{
		UClass* Class = AAssignment_3GameMode::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetElapsedTime", &AAssignment_3GameMode::execGetElapsedTime },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AAssignment_3GameMode_GetElapsedTime_Statics
	{
		struct Assignment_3GameMode_eventGetElapsedTime_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_AAssignment_3GameMode_GetElapsedTime_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Assignment_3GameMode_eventGetElapsedTime_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AAssignment_3GameMode_GetElapsedTime_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AAssignment_3GameMode_GetElapsedTime_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AAssignment_3GameMode_GetElapsedTime_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Assignment_3GameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AAssignment_3GameMode_GetElapsedTime_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AAssignment_3GameMode, nullptr, "GetElapsedTime", nullptr, nullptr, sizeof(Assignment_3GameMode_eventGetElapsedTime_Parms), Z_Construct_UFunction_AAssignment_3GameMode_GetElapsedTime_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AAssignment_3GameMode_GetElapsedTime_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AAssignment_3GameMode_GetElapsedTime_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AAssignment_3GameMode_GetElapsedTime_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AAssignment_3GameMode_GetElapsedTime()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AAssignment_3GameMode_GetElapsedTime_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AAssignment_3GameMode_NoRegister()
	{
		return AAssignment_3GameMode::StaticClass();
	}
	struct Z_Construct_UClass_AAssignment_3GameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ElapsedTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ElapsedTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AAssignment_3GameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Assignment_3,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AAssignment_3GameMode_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AAssignment_3GameMode_GetElapsedTime, "GetElapsedTime" }, // 973206265
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAssignment_3GameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Assignment_3GameMode.h" },
		{ "ModuleRelativePath", "Assignment_3GameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAssignment_3GameMode_Statics::NewProp_ElapsedTime_MetaData[] = {
		{ "Category", "Assignment_3GameMode" },
		{ "ModuleRelativePath", "Assignment_3GameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AAssignment_3GameMode_Statics::NewProp_ElapsedTime = { "ElapsedTime", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AAssignment_3GameMode, ElapsedTime), METADATA_PARAMS(Z_Construct_UClass_AAssignment_3GameMode_Statics::NewProp_ElapsedTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAssignment_3GameMode_Statics::NewProp_ElapsedTime_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AAssignment_3GameMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAssignment_3GameMode_Statics::NewProp_ElapsedTime,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AAssignment_3GameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AAssignment_3GameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AAssignment_3GameMode_Statics::ClassParams = {
		&AAssignment_3GameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AAssignment_3GameMode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AAssignment_3GameMode_Statics::PropPointers),
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_AAssignment_3GameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AAssignment_3GameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AAssignment_3GameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AAssignment_3GameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AAssignment_3GameMode, 2086071546);
	template<> ASSIGNMENT_3_API UClass* StaticClass<AAssignment_3GameMode>()
	{
		return AAssignment_3GameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AAssignment_3GameMode(Z_Construct_UClass_AAssignment_3GameMode, &AAssignment_3GameMode::StaticClass, TEXT("/Script/Assignment_3"), TEXT("AAssignment_3GameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AAssignment_3GameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
