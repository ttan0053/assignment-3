// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Assignment_3Character.h"
#include "Components/SphereComponent.h"
#include "ShooterEnemy.generated.h"

UCLASS()
class ASSIGNMENT_3_API AShooterEnemy : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShooterEnemy();

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* VisibleComponent;

	UPROPERTY(EditAnywhere)
		USphereComponent* ProjectileTriggerVolume;

	UPROPERTY(EditAnywhere)
		USphereComponent* PlayerTriggerVolume;

	UPROPERTY(EditAnywhere)
		float Health;

	UPROPERTY(EditAnywhere)
		float DamageRecieve;

	UPROPERTY(EditAnywhere)
		float DamageOutput;

	UPROPERTY(EditAnywhere)
		UMaterial* HalfMaterial;

	UPROPERTY(EditAnywhere)
		UMaterial* LowMaterial;

	UPROPERTY(EditAnywhere)
		TArray<FVector> Checkpoints;

	UPROPERTY(EditAnywhere)
		float Speed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		USoundBase* DeathSound;

	int32 TargetCheckpoint;

	UPROPERTY(EditAnywhere)
		AAssignment_3Character* MyCharacter;

	bool TargetPlayer = false;
	void SetTargetCheckpoint(int32 target) { TargetCheckpoint = target; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		void OnProjectileOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex,
			bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnPlayerOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex,
			bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnPlayerOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddCheckpoint(FVector checkpoint);

};
