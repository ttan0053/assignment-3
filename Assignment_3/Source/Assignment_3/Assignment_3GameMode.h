// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Assignment_3GameMode.generated.h"

UCLASS(minimalapi)
class AAssignment_3GameMode : public AGameModeBase
{
	GENERATED_BODY()



public:
	AAssignment_3GameMode();

	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
		float ElapsedTime;

	UFUNCTION(BlueprintPure)
		int32 GetElapsedTime() { return int32(ElapsedTime); }

};



