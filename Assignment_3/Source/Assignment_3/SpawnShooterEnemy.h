// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawnShooterEnemy.generated.h"

UCLASS()
class ASSIGNMENT_3_API ASpawnShooterEnemy : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnShooterEnemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
		float SpawnInterval;

	UPROPERTY(EditAnywhere)
		int32 StartTime;

	UPROPERTY(EditAnywhere)
		int MaxNumberOfSpawns;

	UPROPERTY(EditAnywhere, Category = "SpawnObject")
		TSubclassOf<AActor> SpawnObject1;

	UPROPERTY(EditAnywhere)
		TArray<FVector> Checkpoints;


	float SpawnCountdown;
	int AgentCounter;
	bool StartSpawning = false;

	void AdvancedTimer();

	void CountdownHasFinished();

	FTimerHandle StartTimerHandle;
};
