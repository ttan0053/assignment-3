// Fill out your copyright notice in the Description page of Project Settings.


#include "HealPickup.h"

// Sets default values
AHealPickup::AHealPickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//generate object 
	USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	Scene->SetupAttachment(RootComponent);
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visible Component"));
	VisibleComponent->SetupAttachment(Scene);
	TriggerVolume = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Volume"));
	TriggerVolume->SetupAttachment(Scene);

}

// Called when the game starts or when spawned
void AHealPickup::BeginPlay()
{
	Super::BeginPlay();

	//setup collision 
	TriggerVolume->OnComponentBeginOverlap.AddDynamic(this, &AHealPickup::OnPlayerOverlapBegin);
	
}

void AHealPickup::OnPlayerOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex,
	bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && OtherActor != this)
	{
		//if player collsion, when heal them and destroy this
		UE_LOG(LogTemp, Warning, TEXT("Heal"));
		MyCharacter = Cast<AAssignment_3Character>(OtherActor);
		MyCharacter->UpdateHealth((1 - MyCharacter->GetHealth()) * 100);
		Destroy();
	}
}

// Called every frame
void AHealPickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

