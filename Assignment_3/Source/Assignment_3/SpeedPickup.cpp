// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedPickup.h"

// Sets default values
ASpeedPickup::ASpeedPickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//generate object
	USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	Scene->SetupAttachment(RootComponent);
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visible Component"));
	VisibleComponent->SetupAttachment(Scene);
	TriggerVolume = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Volume"));
	TriggerVolume->SetupAttachment(Scene);

	//default values
	MoveSpeedMultiplier = 3;
	EffectTime = 5;

}

// Called when the game starts or when spawned
void ASpeedPickup::BeginPlay()
{
	Super::BeginPlay();
	
	//setup collision 
	TriggerVolume->OnComponentBeginOverlap.AddDynamic(this, &ASpeedPickup::OnPlayerOverlapBegin);
}

void ASpeedPickup::OnPlayerOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex,
	bool bFromSweep, const FHitResult& SweepResult)
{
	//if player collision then give buff and destroy this
	if (OtherActor && OtherActor != this)
	{
		UE_LOG(LogTemp, Warning, TEXT("Speed"));
		MyCharacter = Cast<AAssignment_3Character>(OtherActor);
		//pass the values to the character methods for buff
		MyCharacter->UpdateMoveSpeed(MoveSpeedMultiplier, EffectTime);
		Destroy();
	}
}

// Called every frame
void ASpeedPickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	

}

