// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnShooterEnemy.h"
#include "ShooterEnemy.h"

// Sets default values
ASpawnShooterEnemy::ASpawnShooterEnemy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//set default values
	SpawnInterval = 3;
	StartTime = 3;
	MaxNumberOfSpawns = 2;
	SpawnCountdown = 0;
	AgentCounter = 0;
	UE_LOG(LogTemp, Warning, TEXT("Pass 1"));
}

// Called when the game starts or when spawned
void ASpawnShooterEnemy::BeginPlay()
{
	Super::BeginPlay();

	//set timer
	GetWorldTimerManager().SetTimer(StartTimerHandle, this, &ASpawnShooterEnemy::AdvancedTimer, 1.0f, true);
	UE_LOG(LogTemp, Warning, TEXT("Pass 2"));

}

// Called every frame
void ASpawnShooterEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	int randNum = rand() % Checkpoints.Num();


	SpawnCountdown -= DeltaTime;
	if (SpawnCountdown <= 0)
	{
		//spawning agents
		if (AgentCounter < MaxNumberOfSpawns)
		{
			AShooterEnemy* tempReference1 = GetWorld()->SpawnActor<AShooterEnemy>(SpawnObject1, Checkpoints[randNum], FRotator::ZeroRotator);
			for (FVector checkpoint : Checkpoints)
			{
				tempReference1->AddCheckpoint(checkpoint);
			}
			//set start as the spawn location 
			tempReference1->SetTargetCheckpoint(randNum);
		}

		SpawnCountdown = SpawnInterval;
		AgentCounter++;
	}
	UE_LOG(LogTemp, Warning, TEXT("Pass 3"));

}

void ASpawnShooterEnemy::AdvancedTimer()
{
	//timer
	--StartTime;
	if (StartTime < 1)
	{
		GetWorldTimerManager().ClearTimer(StartTimerHandle);
		CountdownHasFinished();
	}
}

void ASpawnShooterEnemy::CountdownHasFinished()
{
	StartSpawning = true;
}



