// Copyright Epic Games, Inc. All Rights Reserved.

#include "Assignment_3GameMode.h"
#include "Assignment_3HUD.h"
#include "Assignment_3Character.h"
#include "UObject/ConstructorHelpers.h"

AAssignment_3GameMode::AAssignment_3GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AAssignment_3HUD::StaticClass();

	PrimaryActorTick.bCanEverTick = true;
	ElapsedTime = 0.0f;

}

void AAssignment_3GameMode::Tick(float DeltaTime)
{
	ElapsedTime += DeltaTime;
}

