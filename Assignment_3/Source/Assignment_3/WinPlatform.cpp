// Fill out your copyright notice in the Description page of Project Settings.


#include "WinPlatform.h"

// Sets default values
AWinPlatform::AWinPlatform()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//generate object 
	USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	Scene->SetupAttachment(RootComponent);
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visible Component"));
	VisibleComponent->SetupAttachment(Scene);
	TriggerVolume = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Volume"));
	TriggerVolume->SetupAttachment(Scene);

}

// Called when the game starts or when spawned
void AWinPlatform::BeginPlay()
{
	Super::BeginPlay();
	
	//setup collision 
	TriggerVolume->OnComponentBeginOverlap.AddDynamic(this, &AWinPlatform::OnPlayerOverlapBegin);
}

void AWinPlatform::OnPlayerOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex,
	bool bFromSweep, const FHitResult& SweepResult)
{
	//if player collision, open level of the end menu
	if (OtherActor && OtherActor != this)
	{
		UE_LOG(LogTemp, Warning, TEXT("Win"));
		UGameplayStatics::OpenLevel(GetWorld(), "EndMenu");
	}
}

// Called every frame
void AWinPlatform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

