// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterEnemy.h"
#include "Components/StaticMeshComponent.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AShooterEnemy::AShooterEnemy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//generate object 
	USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	Scene->SetupAttachment(RootComponent);
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visible Component"));
	VisibleComponent->SetupAttachment(Scene);
	ProjectileTriggerVolume = CreateDefaultSubobject<USphereComponent>(TEXT("Projectile Trigger Volume"));
	ProjectileTriggerVolume->SetupAttachment(Scene);
	PlayerTriggerVolume = CreateDefaultSubobject<USphereComponent>(TEXT("Player Trigger Volume"));
	PlayerTriggerVolume->SetupAttachment(Scene);

	HalfMaterial = CreateDefaultSubobject<UMaterial>(TEXT("Half Material"));
	LowMaterial = CreateDefaultSubobject<UMaterial>(TEXT("Low Material"));

	ConstructorHelpers::FObjectFinder<UMaterial> HalfMaterialObj(TEXT("/Game/StarterContent/Materials/ShooterEnemyHalf"));
	ConstructorHelpers::FObjectFinder<UMaterial> LowMaterialObj(TEXT("/Game/StarterContent/Materials/ShooterEnemyLow"));

	if (HalfMaterialObj.Succeeded())
	{
		HalfMaterial = HalfMaterialObj.Object;
	}
	if (LowMaterialObj.Succeeded())
	{
		LowMaterial = LowMaterialObj.Object;
	}

	
	


	//set defualt values
	Health = 100.0f;
	DamageRecieve = 20.0f;
	DamageOutput = 4.0f;
	Speed = 1;

}

void AShooterEnemy::BeginPlay()
{
	Super::BeginPlay();

	//setup collision 
	ProjectileTriggerVolume->OnComponentBeginOverlap.AddDynamic(this, &AShooterEnemy::OnProjectileOverlapBegin);
	PlayerTriggerVolume->OnComponentBeginOverlap.AddDynamic(this, &AShooterEnemy::OnPlayerOverlapBegin);
	PlayerTriggerVolume->OnComponentEndOverlap.AddDynamic(this, &AShooterEnemy::OnPlayerOverlapEnd);

}

void AShooterEnemy::OnProjectileOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex,
	bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && OtherActor != this)
	{
		//taking damage
		UE_LOG(LogTemp, Warning, TEXT("Projectile"));
		Health -= DamageRecieve;
	}

	if (Health <= 50 && Health > 20)
	{
		//change material to number 2
		//set material
		VisibleComponent->SetMaterial(0, HalfMaterial);
	}
	else if (Health <= 20 && Health > 0)
	{
		//change to low material
		//set material
		VisibleComponent->SetMaterial(0, LowMaterial);
		UE_LOG(LogTemp, Warning, TEXT("hello"));

	}
	else if (Health <= 0)
	{
		//tigger death
		MyCharacter = Cast<AAssignment_3Character>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		MyCharacter->UpdateKills(1);
		UE_LOG(LogTemp, Warning, TEXT("kill added"));
		Destroy();
	}


}

void AShooterEnemy::OnPlayerOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex,
	bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && OtherActor != this)
	{
		//if player is in radius
		UE_LOG(LogTemp, Warning, TEXT("Player"));
		TargetPlayer = true;
		MyCharacter = Cast<AAssignment_3Character>(OtherActor);

	}

}

void AShooterEnemy::OnPlayerOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex)
{
	//when player leaves radius
	TargetPlayer = false;
}

void AShooterEnemy::AddCheckpoint(FVector checkpoint)
{
	//adds checkpoints 
	Checkpoints.Add(checkpoint);
}

// Called every frame
void AShooterEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (TargetPlayer)
	{
		//change watch direction to next location
		FVector WatchDirection = MyCharacter->GetActorLocation() - this->GetActorLocation();
		WatchDirection.Z = 0;
		WatchDirection.Normalize();

		FVector Forward = FVector(1, 0, 0);

		float Dot = FVector::DotProduct(Forward, WatchDirection);
		float Det = Forward.X * WatchDirection.Y + Forward.Y * WatchDirection.X;
		float Rad = FMath::Atan2(Det, Dot);
		float Degrees = FMath::RadiansToDegrees(Rad);

		this->SetActorRotation(FRotator(0, Degrees, 180));
	}

	//move/lerp to next location
	RootComponent->SetWorldLocation(FMath::Lerp(this->GetActorLocation(), Checkpoints[TargetCheckpoint], DeltaTime * Speed));

	if (FVector::Dist(this->GetActorLocation(), Checkpoints[TargetCheckpoint]) < 2.0f)
	{
		TargetCheckpoint++;
		if (TargetCheckpoint > Checkpoints.Num() - 1)
			TargetCheckpoint = 0;
	}

}

