// Fill out your copyright notice in the Description page of Project Settings.

#include "SpawnRadiusEnemy.h"

// Sets default values
ASpawnRadiusEnemy::ASpawnRadiusEnemy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//set default values
	SpawnInterval = 3;
	StartTime = 3;
	MaxNumberOfSpawns = 2;
	SpawnCountdown = 0;
	AgentCounter = 0;
	UE_LOG(LogTemp, Warning, TEXT("Pass 1"));
}

// Called when the game starts or when spawned
void ASpawnRadiusEnemy::BeginPlay()
{
	Super::BeginPlay();

	//set timer
	GetWorldTimerManager().SetTimer(StartTimerHandle, this, &ASpawnRadiusEnemy::AdvancedTimer, 1.0f, true);
	UE_LOG(LogTemp, Warning, TEXT("Pass 2"));
	
}

// Called every frame
void ASpawnRadiusEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	int randNum = rand() % Checkpoints.Num();

	//countdown
	SpawnCountdown -= DeltaTime;
	if (SpawnCountdown <= 0)
	{
		//spawning agents
		if (AgentCounter < MaxNumberOfSpawns)
		{
			ARadiusEnemy* tempReference1 = GetWorld()->SpawnActor<ARadiusEnemy>(SpawnObject1, Checkpoints[randNum], FRotator::ZeroRotator);
			for (FVector checkpoint : Checkpoints)
			{
				tempReference1->AddCheckpoint(checkpoint);
			}
			//set start as the spawn location 
			tempReference1->SetTargetCheckpoint(randNum);
		}

		SpawnCountdown = SpawnInterval;
		AgentCounter++;
	}
	UE_LOG(LogTemp, Warning, TEXT("Pass 3"));

}

void ASpawnRadiusEnemy::AdvancedTimer()
{
	//timer
	--StartTime;
	if (StartTime < 1)
	{
		GetWorldTimerManager().ClearTimer(StartTimerHandle);
		CountdownHasFinished();
	}
}

void ASpawnRadiusEnemy::CountdownHasFinished()
{
	StartSpawning = true;
}

