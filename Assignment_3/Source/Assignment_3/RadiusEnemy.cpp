// Fill out your copyright notice in the Description page of Project Settings.

#include "RadiusEnemy.h"
#include "Components/StaticMeshComponent.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ARadiusEnemy::ARadiusEnemy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	//generate object 
	USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	Scene->SetupAttachment(RootComponent);
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visible Component"));
	VisibleComponent->SetupAttachment(Scene);
	ProjectileTriggerVolume = CreateDefaultSubobject<USphereComponent>(TEXT("Projectile Trigger Volume"));
	ProjectileTriggerVolume->SetupAttachment(Scene);
	PlayerTriggerVolume = CreateDefaultSubobject<USphereComponent>(TEXT("Player Trigger Volume"));
	PlayerTriggerVolume->SetupAttachment(Scene);

	HalfMaterial = CreateDefaultSubobject<UMaterial>(TEXT("Half Material"));
	LowMaterial = CreateDefaultSubobject<UMaterial>(TEXT("Low Material"));

	ConstructorHelpers::FObjectFinder<UMaterial> HalfMaterialObj(TEXT("/Game/StarterContent/Materials/RadiusEnemyHalf"));
	ConstructorHelpers::FObjectFinder<UMaterial> LowMaterialObj(TEXT("/Game/StarterContent/Materials/RadiusEnemyLow"));

	//setup other meshes
	if (HalfMaterialObj.Succeeded())
	{
		HalfMaterial = HalfMaterialObj.Object;
	}
	if (LowMaterialObj.Succeeded())
	{
		LowMaterial = LowMaterialObj.Object;
	}


	//default values
	Health = 100.0f;
	DamageRecieve = 20.0f;
	DamageOutput = 4.0f;
	ElapsedTime = 0.0f;
	Speed = 1;


}

// Called when the game starts or when spawned
void ARadiusEnemy::BeginPlay()
{
	Super::BeginPlay();

	//setup collision
	ProjectileTriggerVolume->OnComponentBeginOverlap.AddDynamic(this, &ARadiusEnemy::OnProjectileOverlapBegin);
	PlayerTriggerVolume->OnComponentBeginOverlap.AddDynamic(this, &ARadiusEnemy::OnPlayerOverlapBegin);
	PlayerTriggerVolume->OnComponentEndOverlap.AddDynamic(this, &ARadiusEnemy::OnPlayerOverlapEnd);
	
}

void ARadiusEnemy::OnProjectileOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex,
	bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && OtherActor != this)
	{
		//if shotm take damage
		UE_LOG(LogTemp, Warning, TEXT("Projectile"));
		Health -= DamageRecieve;
	}
	
	if (Health <= 50 && Health > 20)
	{
		//change material to number 2
		//set material
		VisibleComponent->SetMaterial(0, HalfMaterial);
	}
	else if (Health <= 20 && Health > 0)
	{
		//change to low material
		//set material
		VisibleComponent->SetMaterial(0, LowMaterial);
		UE_LOG(LogTemp, Warning, TEXT("hello"));

	}
	else if (Health <= 0)
	{
		//tigger death
		MyCharacter = Cast<AAssignment_3Character>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		MyCharacter->UpdateKills(1);
		UE_LOG(LogTemp, Warning, TEXT("kill added"));
		if (DeathSound != nullptr)
		{
			UGameplayStatics::PlaySoundAtLocation(this, DeathSound, GetActorLocation());

		}
		//if health is less than zero destroy this and make explosion sound
		Destroy();
	}

	
}

void ARadiusEnemy::OnPlayerOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex,
	bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && OtherActor != this)
	{
		//damage player if in range
		UE_LOG(LogTemp, Warning, TEXT("Player"));
		DealTickDamage = true;
		MyCharacter = Cast<AAssignment_3Character>(OtherActor);
	
	}

}

void ARadiusEnemy::OnPlayerOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex)
{
	//when player leaves radius
	DealTickDamage = false;
}

void ARadiusEnemy::AddCheckpoint(FVector checkpoint)
{
	//adds the checkpoints
	Checkpoints.Add(checkpoint);
}

// Called every frame
void ARadiusEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ElapsedTime += DeltaTime;
	
	//deal tick damage once per second if in range
	if (DealTickDamage == true && ElapsedTime > 1)
	{
		UE_LOG(LogTemp, Warning, TEXT("Player damaged"));
		MyCharacter->UpdateHealth(-1 * DamageOutput);

		ElapsedTime = 0.0f;
	}
	
	//move/lerp to next location
	RootComponent->SetWorldLocation(FMath::Lerp(this->GetActorLocation(), Checkpoints[TargetCheckpoint], DeltaTime * Speed));

	if (FVector::Dist(this->GetActorLocation(), Checkpoints[TargetCheckpoint]) < 2.0f)
	{
		TargetCheckpoint++;
		if (TargetCheckpoint > Checkpoints.Num()-1)
			TargetCheckpoint = 0;
	}

}

